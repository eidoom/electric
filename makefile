.PHONY: all

all: notes.pdf

notes.pdf: notes.md correlation.pdf consumption.pdf average_daily_consumption.pdf average_slot_consumption.pdf average_monthly_consumption.pdf price_by_slot.pdf prices.pdf
	pandoc $< -o $@

consumption.pdf: plot_consumption.py consumption.json
	pipenv run python3 $<

consumption.json: get_last_year_consumption.py
	pipenv run python3 $<

correlation.pdf: plot_correlation.py price_old_agile.json consumption.json
	pipenv run python3 $<

price_old_agile.json: get_prices.py
	pipenv run python3 $<

average_monthly_consumption.pdf: plot_average_monthly_consumption.py
	pipenv run python3 $<

average_daily_consumption.pdf: plot_average_daily_consumption.py
	pipenv run python3 $<

average_slot_consumption.pdf: plot_average_slot_consumption.py
	pipenv run python3 $<

price_by_slot.pdf: plot_price_by_slot.py
	pipenv run python3 $<

prices.pdf: plot_prices.py
	pipenv run python3 $<
