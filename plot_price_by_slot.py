#!/usr/bin/env python3

import datetime
import json

import matplotlib.pyplot
import numpy

from plot_average_slot_consumption import clock24

if __name__ == "__main__":
    with open("price_old_agile.json", "r") as f:
        old_tariff = json.load(f)["results"]

    prices = numpy.array([d["value_inc_vat"] for d in old_tariff])

    times = numpy.array([clock24(d["valid_from"]) for d in old_tariff])

    clock = numpy.arange(0, 24, 0.5)

    hist = numpy.array([prices[times == t] for t in clock])

    mean = numpy.mean(hist, axis=1)

    fig, ax = matplotlib.pyplot.subplots()

    ax.set_xticks(numpy.arange(0, 25, 2))
    ax.set_xticks(numpy.arange(0, 25, 1), minor=True)

    ax.set_xlabel("Time of day (hours)")
    ax.set_ylabel("Mean price (p/kWh)")

    edges = numpy.arange(0, 24.5, 0.5)

    ax.stairs(mean, edges, fill=True)

    for x in (16, 19):
        ax.axvline(x, color="red")

    fig.savefig("price_by_slot.pdf", bbox_inches="tight")
