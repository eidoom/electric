#!/usr/bin/env python3

import datetime
import json
import math

import matplotlib.colors
import matplotlib.pyplot
import numpy

from calc_cost import timeify

if __name__ == "__main__":
    with open("consumption.json", "r") as f:
        consumption = json.load(f)["results"]

    with open("price_old_agile.json", "r") as f:
        tariff = json.load(f)["results"][::-1]

    data = {timeify(d["valid_from"]): {"price": d["value_inc_vat"]} for d in tariff}

    for d in consumption:
        data[timeify(d["interval_start"])]["usage"] = d["consumption"]

    data = {k: v for k, v in data.items() if "usage" in v}

    # times = data.keys()
    # start = min(data.keys())
    # end = max(data.keys())
    # n_slots = (end - start).total_seconds() / (60 * 30)

    data = data.values()

    price = [d["price"] for d in data]
    usage = [d["usage"] for d in data]

    # align bins to the penny
    low = math.floor(min(price))
    high = math.ceil(max(price))
    bins = numpy.histogram_bin_edges(price, range=(low, high), bins=high - low)

    fig, ax = matplotlib.pyplot.subplots()
    ax.set_xlabel("Price (p/kWh)")
    ax.set_ylabel("Usage (kWh per year)")

    ax.hist(
        price,
        weights=usage,
        bins=bins,
        log=False,
    )

    fig.savefig("correlation1d.pdf", bbox_inches="tight")
