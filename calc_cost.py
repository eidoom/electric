#!/usr/bin/env python3

import datetime
import json


def timeify(dt):
    return datetime.datetime.fromisoformat(dt).astimezone(datetime.timezone.utc)


if __name__ == "__main__":
    with open("consumption.json", "r") as f:
        consumption = json.load(f)["results"]

    with open("price_old_agile.json", "r") as f:
        old_tariff = json.load(f)

    old_standing_charge = old_tariff["standing_charge"]
    old_tariff = old_tariff["results"][::-1]

    with open("price_new_agile.json", "r") as f:
        new_tariff = json.load(f)

    new_standing_charge = new_tariff["standing_charge"]
    new_tariff = new_tariff["results"][::-1]

    # n = len(new_tariff)
    # old_tariff = old_tariff[:n]

    data = {
        timeify(d["valid_from"]): {"new_price": d["value_inc_vat"]} for d in new_tariff
    }

    for d in new_tariff:
        try:
            data[timeify(d["valid_from"])]["old_price"] = d["value_inc_vat"]
        except KeyError:
            pass

    for d in consumption:
        try:
            data[timeify(d["interval_start"])]["usage"] = d["consumption"]
        except KeyError:
            pass

    dts = data.keys()
    days = (max(dts) - min(dts)).days
    print(f"{days} days")

    kwhs = sum(a["usage"] for a in data.values() if "usage" in a)

    print(f"{int(kwhs)} kWh")

    old_agile = (
        days * old_standing_charge
        + sum(a["old_price"] * a["usage"] for a in data.values() if "usage" in a)
    ) / 100
    print(f"old agile: £{old_agile:.2f}")

    new_agile = (
        days * new_standing_charge
        + sum(a["new_price"] * a["usage"] for a in data.values() if "usage" in a)
    ) / 100
    print(f"new agile: £{new_agile:.2f}")

    new_flexible = (days * 57.33 + kwhs * 49.96) / 100
    print(f"new flexible worst: £{new_flexible:.2f}")

    new_flexible_best = (days * 57.33 + kwhs * 32.56) / 100
    print(f"new flexible best: £{new_flexible_best:.2f}")
