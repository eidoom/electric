#!/usr/bin/env python3

import datetime
import json

import matplotlib.pyplot
import numpy

if __name__ == "__main__":
    with open("price_old_agile.json", "r") as f:
        old_tariff = json.load(f)["results"]

    n = len(old_tariff)

    prices = numpy.array([d["value_inc_vat"] for d in old_tariff])

    fig, ax = matplotlib.pyplot.subplots()

    ax.set_yscale("log")

    ax.set_xlabel("Price (p/kWh)")
    ax.set_ylabel("Proportion")

    bins = numpy.histogram_bin_edges(prices, bins="auto")

    ax.hist(prices, bins=bins, weights=[1 / n] * n)

    fig.savefig("prices.pdf", bbox_inches="tight")
