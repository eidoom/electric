#!/usr/bin/env python3

import datetime
import json

import matplotlib.colors
import matplotlib.pyplot

from calc_cost import timeify

if __name__ == "__main__":
    with open("consumption.json", "r") as f:
        consumption = json.load(f)["results"]

    with open("price_old_agile.json", "r") as f:
        tariff = json.load(f)["results"][::-1]

    data = {timeify(d["valid_from"]): {"price": d["value_inc_vat"]} for d in tariff}

    for d in consumption:
        data[timeify(d["interval_start"])]["usage"] = d["consumption"]

    data = data.values()

    price = [d["price"] for d in data if "usage" in d]
    usage = [d["usage"] for d in data if "usage" in d]

    fig, ax = matplotlib.pyplot.subplots()
    ax.set_xlabel("Price (p/kWh)")
    ax.set_ylabel("Usage (kWh per half-hour slot)")

    # ax.hexbin(
    #     price,
    #     usage,
    #     gridsize=30,
    #     bins='log',
    #     norm=matplotlib.colors.LogNorm(),
    # )

    h, xedges, yedges, image = ax.hist2d(
        price,
        usage,
        bins=30,
        norm=matplotlib.colors.LogNorm(),
        density=True,
    )

    ax.vlines(0, 0, max(usage), color="red")

    cbar = fig.colorbar(image, label="Density")

    fig.savefig("correlation.pdf", bbox_inches="tight")
