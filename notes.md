# Electricity usage

The following data is from over the last year.

## Correlation

![](correlation.pdf)

This shows the correlation between electricity usage and its price.
The zero point of the price is shown by the red line; you receive money for usage to the left of it!
For optimal usage, you want the upper left region to have the highest correlation density---however, of course, the opposite is true because there's a reason the price goes up...

## Consumption

### Average usage per half-hour slot of the day

![](average_slot_consumption.pdf)

The peak hours are outlined in red.

The daily routine is quite clear to see!

### Average usage by day of the week

![](average_daily_consumption.pdf)

You tend to use a bit more electricity on the weekend.

### Usage usage by month of the year

![](average_monthly_consumption.pdf)

It's the outside temperature, and therefore heating, that we see making a big difference here.

### All usage

![](consumption.pdf)

This one doesn't fit on the page.

Notice that 3 days are missing: 20, 21, and 23 Dec 2022.
That's on Octopus.

The red lines outline the peak hours, which come with peak cost.

Winter heating is clearly visible.

## Prices

### Distribution of prices

![](prices.pdf)

### Average prices by half-hour slot of the day

![](price_by_slot.pdf)
