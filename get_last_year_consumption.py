#!/usr/bin/env python3

import datetime
import json
import urllib.parse

import requests


def chop_datetime(dt):
    return dt.replace(hour=0, minute=0, second=0, microsecond=0)


def zulu(dt):
    return dt.isoformat(timespec="minutes").replace("+00:00", "Z")


if __name__ == "__main__":
    days = 365.25

    today = datetime.datetime.now(datetime.timezone.utc)
    today = chop_datetime(today)

    yesteryear = today - datetime.timedelta(days=days)
    yesteryear = chop_datetime(yesteryear)

    number = int(days * 48)

    with open("details.json", "r") as f:
        details = json.load(f)

    url = f"https://api.octopus.energy/v1/electricity-meter-points/{details['mpan']}/meters/{details['serial']}/consumption/?page_size={number}&period_from={zulu(yesteryear)}&period_to={zulu(today)}&order_by=period"

    url = urllib.parse.urlunparse(urllib.parse.urlparse(url))

    r = requests.get(url, auth=(details["apikey"], ""))

    output = r.json()

    with open("consumption.json", "w") as f:
        json.dump(output, f)
