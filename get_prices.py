#!/usr/bin/env python3

import datetime
import json

import requests

from get_last_year_consumption import chop_datetime, zulu


def get_prices(region, product_code, filename):
    days = 365.25

    today = datetime.datetime.now(datetime.timezone.utc)
    today = chop_datetime(today)

    yesteryear = today - datetime.timedelta(days=days)
    yesteryear = chop_datetime(yesteryear)

    url0 = f"https://api.octopus.energy/v1/products/{product_code}/electricity-tariffs/E-1R-{product_code}-{region}/standing-charges/"
    r0 = requests.get(url0)
    output0 = r0.json()
    standing_charge = output0["results"][0]["value_inc_vat"]

    url = f"https://api.octopus.energy/v1/products/{product_code}/electricity-tariffs/E-1R-{product_code}-{region}/standard-unit-rates/?period_from={zulu(yesteryear)}&period_to={zulu(today)}&page_size=1500"

    final = {"results": [], "standing_charge": standing_charge}

    while url:
        r = requests.get(url)
        output = r.json()
        final["results"] += output["results"]
        url = output["next"]

    with open(f"{filename}.json", "w") as f:
        json.dump(final, f)


if __name__ == "__main__":
    region = "P"

    # old tariff
    get_prices(region, "AGILE-18-02-21", "price_old_agile")

    # new tariff
    get_prices(region, "AGILE-FLEX-22-11-25", "price_new_agile")
