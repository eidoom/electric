#!/usr/bin/env python3

import datetime
import json

import matplotlib.pyplot
import numpy

if __name__ == "__main__":
    with open("consumption.json", "r") as f:
        data = json.load(f)["results"]

    daily_slots = 48

    # number_days = 354
    # data = data[142:]  # start on a Monday
    # data = data[: daily_slots * number_days]  # end last Sunday

    number_days = len(data) // daily_slots

    consumption = numpy.array([d["consumption"] for d in data])  # kWh
    dts = numpy.array(
        [
            datetime.datetime.fromisoformat(d["interval_start"])
            # .astimezone(tz=datetime.timezone.utc)
            for d in data
        ]
    )

    times = numpy.array(
        [
            (
                datetime.datetime.fromtimestamp(0, tz=datetime.timezone.utc)
                + datetime.timedelta(hours=i)
            ).strftime("%H:%M")
            for i in range(24)
        ]
    )

    days = numpy.array([d.strftime("%a %d %b %Y") for d in dts[::daily_slots]])

    consumption_by_day = numpy.split(consumption, number_days)

    w = 70
    fig, ax = matplotlib.pyplot.subplots(figsize=(w, w))

    ax.set_xticks(numpy.arange(0, daily_slots, 2), times)
    ax.set_xticks(numpy.arange(0, daily_slots), minor=True)
    ax.tick_params(axis="x", labelrotation=90)

    ax.set_yticks(numpy.arange(number_days), days[::-1])

    im = ax.imshow(consumption_by_day, extent=(0, daily_slots, -0.5, number_days - 0.5))

    ax.vlines((32, 38), -0.5, number_days - 0.5, color="red")

    cbar = fig.colorbar(
        im, fraction=0.002, pad=0.002, label="kWh used in half-hour slot"
    )

    fig.savefig("consumption.pdf", bbox_inches="tight")
