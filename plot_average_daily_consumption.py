#!/usr/bin/env python3

import calendar
import datetime
import json

import matplotlib.pyplot
import numpy

if __name__ == "__main__":
    with open("consumption.json", "r") as f:
        data = json.load(f)["results"]

    daily_slots = 48
    number_weeks = len(data) / daily_slots / 7

    consumption = numpy.array([d["consumption"] for d in data]) / number_weeks
    weeks = numpy.array(
        [datetime.datetime.fromisoformat(d["interval_start"]).weekday() for d in data]
    )

    hist, bin_edges = numpy.histogram(weeks, bins=7, range=(0, 7), weights=consumption)

    fig, ax = matplotlib.pyplot.subplots()

    ax.bar(calendar.day_abbr, hist)

    ax.set_xlabel("Day")
    ax.set_ylabel("Mean daily consumption (kWh)")

    fig.savefig("average_daily_consumption.pdf", bbox_inches="tight")
