#!/usr/bin/env python3

import json

import requests

if __name__ == "__main__":
    with open("details.json", "r") as f:
        details = json.load(f)

    url = f"https://api.octopus.energy/v1/accounts/{details['account']}/"

    r = requests.get(url, auth=(details["apikey"], ""))

    output = r.json()

    with open("account.json", "w") as f:
        json.dump(output, f)
