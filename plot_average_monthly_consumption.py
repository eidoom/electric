#!/usr/bin/env python3

import calendar
import datetime
import json

import matplotlib.pyplot
import numpy

if __name__ == "__main__":
    with open("consumption.json", "r") as f:
        data = json.load(f)["results"]

    # daily_slots = 48
    # number_years = len(data) / daily_slots / 365.25
    number_years = 1

    consumption = numpy.array([d["consumption"] for d in data]) / number_years
    months = numpy.array(
        [datetime.datetime.fromisoformat(d["interval_start"]).month for d in data]
    )

    hist, bin_edges = numpy.histogram(
        months, bins=12, range=(1, 13), weights=consumption
    )

    fig, ax = matplotlib.pyplot.subplots()

    ax.bar([*calendar.month_abbr][1:], hist)

    ax.set_xlabel("Month")
    ax.set_ylabel("Monthly consumption (kWh)")

    fig.savefig("average_monthly_consumption.pdf", bbox_inches="tight")
