#!/usr/bin/env python3

import datetime
import json

import matplotlib.pyplot
import numpy


def clock24(isodtstr):
    t = datetime.datetime.fromisoformat(isodtstr).time()
    return t.hour + t.minute / 60


if __name__ == "__main__":
    with open("consumption.json", "r") as f:
        data = json.load(f)["results"]

    daily_slots = 48
    number_days = len(data) / daily_slots

    consumption = numpy.array([d["consumption"] for d in data]) / number_days
    times = numpy.array([clock24(d["interval_start"]) for d in data])

    fig, ax = matplotlib.pyplot.subplots()

    # a histogram is the lazy way to handle different time zones
    # ie. can't just numpy.sum(numpy.split(consumption, daily_slots), axis=1) because of daylight saving!
    bins = numpy.histogram_bin_edges(times, bins=48, range=(0, 24))
    ax.hist(times, weights=consumption, bins=bins, histtype="stepfilled")

    ax.set_xticks(numpy.arange(0, 25, 2))
    ax.set_xticks(numpy.arange(0, 25, 1), minor=True)

    ax.set_xlabel("Time of day (hours)")
    ax.set_ylabel("Mean consumption in half-hour slot (kWh)")

    for x in (16, 19):
        ax.axvline(x, color="red")

    fig.savefig("average_slot_consumption.pdf", bbox_inches="tight")
